
function getValuesRange() {

    var p = document.getElementById("multi3"),
        res = document.getElementById("numerozoom");
    p.value = 1000;
    $('#numerozoom').text("Pies de altura: "+p.value);
    p.addEventListener("input", function() {
        res.innerHTML = "Pies de altura :" + p.value;
        colorBadgeController(p.value);
        zoomCesium(p.value);

    }, false);
}
function colorBadgeController(value)
{
    if(parseInt(value) > 50000 && parseInt(value) < 100000)
    {
        $('#numerozoom').removeClass('badge-success badge-danger');
        $('#numerozoom').addClass('badge-warning');
    }else if(parseInt(value) > 100000)
    {
        $('#numerozoom').removeClass('badge-warning');
        $('#numerozoom').addClass('badge-danger');

    }
    else if(parseInt(value) < 50000)
    {
        $('#numerozoom').removeClass('badge-warning');
        $('#numerozoom').addClass('badge-success');
    }
}
function disableScrollZoomCesium()
{
    var pageStep = 25;
    document.getElementById("cesiumContainer").addEventListener("wheel", function(event) {
        if (event.deltaY < 0) {
            window.scroll(0, window.pageYOffset - pageStep);
        } else {
            window.scroll(0, window.pageYOffset + pageStep);
        }
    });
}
async function SunMoon(movement,opacit)
{
    $( "#visibilidad" ).animate({
        left:movement,
        opacity:opacit        
    }, 4000, function() {

     
  });
}
function styleQuerie()
{
    if($(window).width()  > 767 && $(window).width() < 1200)
    {
        $('#cesium').removeClass('col-md-5');
        $('#cesium').addClass('col-md-10');        
    }
}
function cookiesInfo(loc){

    //alert($.cookie("example",loc));
     $('#locations').append($.cookie("locations",loc));
}

function noConnection()
{
    $(body).text("no hay conexión");
}

$(window).bind('resize', function(e)
{
    if (window.RT) clearTimeout(window.RT);
    window.RT = setTimeout(function()
    {
        this.location.reload(false); /* false to get page from cache */

    }, 100);
});
$(document).on('ready',function(){
   /* $(document)
          .ajaxStart(function () {
               $("#cargando").text('cargando.....');
          })*/
    if(navigator.onLine) {
        CesiumContainer();
        moveISS();
        PeopleIn();
        getValuesRange();
        disableScrollZoomCesium();
        styleQuerie();
    } else {
        noConnection()
    }


   // map3d.getCesiumScene().screenSpaceCameraController.enableZoom = false;
});