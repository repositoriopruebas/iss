function moveISS () {
    $.getJSON('https://api.wheretheiss.at/v1/satellites/25544', function(data) {
        lat = data.latitude;
        lon = data.longitude;
        $('#lat').text("Lat: "+data.latitude.toFixed(2));
        $('#lon').text("Lon: "+data.longitude.toFixed(2));
        $('#solarlat').text("Lat sun: "+data.solar_lat.toFixed(2));
        $('#solarlon').text("Lon sun: "+data.solar_lon.toFixed(2));
        $('#velocity').text(data.velocity.toFixed(2)+" kms/h");
        $('#altitude').text("Altitud kms: "+data.altitude.toFixed(2));
        $('#visibility').text(data.visibility);

        if(daynight != data.visibility)
        {
             if(data.visibility == "eclipsed" && daynight)
             {
                SunMoon('200px',0).then(function(){

                    $('#visibility').html('<img width="10%;" src="https://image.flaticon.com/icons/svg/3226/3226413.svg">');
                });  
                SunMoon('0px',1).then(function(){
                    $('#visibility').html('<img width="10%;" src="https://image.flaticon.com/icons/svg/979/979585.svg">');
                }); 
             }else if (data.visibility == "daylight" && daynight)
             {
                SunMoon('200px',0).then(function(){

                    $('#visibility').html('<img width="10%;" src="https://image.flaticon.com/icons/svg/979/979585.svg">');
                });  
                SunMoon('0px',1).then(function(){
                    $('#visibility').html('<img width="10%;" src="https://image.flaticon.com/icons/svg/3226/3226413.svg">');

                });     
             }           
        }
        daynight = data.visibility;
        if(data.visibility == "eclipsed")
        {
            $('.jumbotron').fadeIn(4000,function(){
                $('.jumbotron').css('background-color','#252850');
            });
            $('h1').css('color','white');
            $('.jumbotron').css('color','#ccffcc');
            $('#data').css('color','#ccffcc');
            $('body').css('background-color','grey');
            $('#visibility').html('<img width="10%;" src="https://image.flaticon.com/icons/svg/3226/3226413.svg">');       
        }else
        {
            $('.jumbotron').fadeIn(4000,function(){
                $('.jumbotron').css('background-color','#bbdefb');
                $('.jumbotron').css('background-color','#bbdefb');
                $('.jumbotron').css('color','black');
                $('#data').css('color','black');
                $('h1').css('color','black');
            });
            
            $('#visibility').html('<img width="10%;" src="https://image.flaticon.com/icons/svg/979/979585.svg">');           
        }
        maping(lat,lon);
        setTimeout(moveISS,time);
        findAdress(lat,lon);
        GeoTemperature(lat,lon);
        // passNext(lat,lon);
    });
}

function GeoTemperature(lat,lon)
{
    $.getJSON('http://api.openweathermap.org/data/2.5/weather?lat='+lat+'&lon='+lon+'&units=metric&appid=13b74dab2aa39016e415edc7b32b02d2',function(response){
        console.log(response);
        $('#temperatura').text("temp: "+response.main.temp+"°");
        $('#descripcion').text("desc: "+response.weather[0].description);
    });
}
function passNext(lat,lon)
{
    $.getJSON('http://api.open-notify.org/iss-pass.json?lat='+lat+'&lon='+lon+'',function(data){
        console.log(data);
    });
}
function getMoreInfoIss(lat,lon)
{
    var today = new Date().toISOString().split('T')[0];
    //api key nasa 2dBaTefmAVwoJW1bhJSD07LPDDhmxJA8aMY5nQpe
    $.getJSON('https://api.wheretheiss.at/v1/coordinates/'+lat+','+lon+'',function(data){
        console.log(data);
        infoCountry(data.country_code);
        $('#timezone').html("<br>Timezone: "+data.timezone_id);
    });
}
function findAdress(lat,lon)
{
    $.getJSON("http://nominatim.openstreetmap.org/reverse?" +
        "format=json&addressdetails=0&zoom=18&" +
        "lat=" + lat + "&lon=" + lon + "&json_callback=?",function(response){
        if(response.display_name)
        {
            time = 8000;
            var info = response.display_name.split(',');
            $('#data').html('<span>'+info+'</span>');
            CesiumCoordinates(lat,lon,'');
            getMoreInfoIss(lat,lon);
            $('#contenedortle').hide(800);
            $('#cesiumContainer').show(800);
            $('#map').hide(800);
            cookiesInfo(response.display_name);
            //infoCountry(info[info.length-1]);
        }else{
            time = 60000;
            $.getJSON("https://api.opencagedata.com/geocode/v1/json?q="+lat+"+"+lon+"&key=458731db735f4b2ea8d2fda1c5f5ab52",function(response){
                console.log(response);
                $('#flag').show();
                $('#flag').attr('src','https://image.flaticon.com/icons/svg/3075/3075404.svg');
                $('#cesiumContainer').hide(800);
                $('#data').text(response.results[0].components.body_of_water);
                $('#remaining').text("Peticiones pendientes: "+response.rate.remaining);
                $('#map').show(800);
                $('#contenedortle').show(400);          
               // cookiesInfo(response.results[0].components.body_of_water);         
            })
        }
    });
}
function infoCountry(country)
{
    request = $.ajax({
        url: "https://restcountries.eu/rest/v2/alpha/"+country+"",
        method: "GET"
    });
    request.done(function( data ) {
        console.log(data);
        var response = JSON.parse(JSON.stringify(data));
        console.log(response.flag);
        $('#flag').css('display','inline');
        $('#flag').attr('src',response.flag);
    });
}
function PeopleIn()
{
    $.getJSON('http://api.open-notify.org/astros.json?callback=?', function(data) {
        var number = data['number'];
        $('#spacepeeps').html(number);
        data['people'].forEach(function (d) {
            $('#astronames').append('<li>' + d['name'] + '</li>');
        });
    });
}