var norad_n2yo = '25544|ISS';
var size_n2yo = resizeMapWidth();
var allpasses_n2yo = '1';
var map_n2yo = '2';
/*sitios de interés
https://stackoverrun.com/es/q/1716621
* */
var map;
var time = 4000;
var viewer;
var lat;
var lon;
var daynight = '';
//13b74dab2aa39016e415edc7b32b02d2 api key temperaturas 1d81a6525e4417b3573673f74f2715c7

function maping(lat,lon)
{
    if(map != undefined || map != null){
        map.remove();
    }
    var Icono = L.icon({
        iconUrl: "https://image.flaticon.com/icons/svg/81/81959.svg",
        iconSize: [60, 70],
        iconAnchor: [15, 40],
        popupAnchor: [0, -40]
    });
    map = L.map('map')
        .setView([lat, lon],
            4);
    L.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
        attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://cloudmade.com">CloudMade</a>',
        maxZoom: 18
    }).addTo(map);
    L.marker([lat,lon],{icon: Icono}).addTo(map);
    L.control.scale().addTo(map);
}

async function CesiumContainer()
{
    Cesium.Ion.defaultAccessToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI5NjQ2ZGYyMC05MWM4LTQ4YTUtYmYwNy0xYWRhNGY2ZGIxNzYiLCJpZCI6MzIzMzQsInNjb3BlcyI6WyJhc3IiLCJnYyJdLCJpYXQiOjE1OTY3ODM1MDh9.Gl8PQCXzj3ZElfqADmn-vjjQo6aZ_ml-IYOi8YH-jUU";
    viewer = new Cesium.Viewer('cesiumContainer',{
        // Desactivar el widget de la pelota inferior izquierda.
        animation: false,
        // Desactivar el widget de la barra temporal inferior.
        timeline: false,
    });
}
function CesiumCoordinates(lat,lon,mode)
{
    if(mode == ''){
        mode =  $('#multi3').val();
    }
    if($('#multi3').val() == '')
    {
        mode = 1000;
    }
    viewer.camera.setView({
        destination : Cesium.Cartesian3.fromDegrees(lon,lat, mode)
    });
}
function zoomCesium(value){
    console.log(value);
    CesiumCoordinates(lat,lon, value)
}

function resizeMapWidth()
{
    if($(window).width() > 1700)
    {
        $('#map').css('height','500px');
        $
        return "large";
    }
    else if($(window).width() > 1200 && $(window).width() < 1700 )
    {
        return "medium";
    }else if($(window).width() > 767 && $(window).width() < 960)
    {
        return "medium";
    }else if($(window).width() > 960 && $(window).width() < 1027)
    {
        return "large";
    }
    else if($(window).width() > 320 && $(window).width() < 480)
    {
        return "small";
    }
}